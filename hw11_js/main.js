const buttons = document.querySelectorAll('.btn');

function onKeyPress(event) {
    /*//нажатая кнопка приведенная к нижнему регистру. в переменной для дальнейшего сравнения*/
    const press = event.key.toLowerCase();
    buttons.forEach(function (element) {
        /*текст внутри елемента (кнопка) приведен к нижнему регистру*/
        const btnText = element.innerText.toLowerCase();
        /* сначала проверяем на наличие класса и удаляем его*/
        if (element.classList.contains('coloring')) {
            element.classList.remove('coloring');
        }
        /*добавляем класс с окрашиванием*/
        if (press === btnText) {
            element.classList.add('coloring');
        }
    })
}
document.querySelector("body").addEventListener('keypress', onKeyPress);
