/*Our services section*/
const tabsServices = document.querySelector('.menu-hover');
const servicesContent = document.querySelector('.tab-content');

function onClickTabs(event) {
    const tabsTitle = document.querySelectorAll('.menu-hover>.tab-title');
    const tabText = document.querySelectorAll('.tab-text');

    const tabColorChange = tabsServices.querySelector('.active');
    tabColorChange.classList.remove('active');
    event.target.classList.add('active');

    const textVisibilityChange = servicesContent.querySelector('.visible');
    textVisibilityChange.classList.remove('visible');

    for (let i = 0; i < tabsTitle.length; i++) {
        if (tabsTitle.item(i).classList.contains('active')) {
            tabText.item(i).classList.add('visible');
            break;
        }
    }
}

tabsServices.addEventListener('click', onClickTabs);

/* our amazing work  + loader with dots*/
const loadButton = document.querySelector('.load-button');
const container = document.querySelector('.grid-container');
const hidePics = [
    ['page1', 'graphic'],
    ['page2', 'web'],
    ['page3', 'landing'],
    ['page4', 'wordpress'],
    ['page5', 'graphic'],
    ['page6', 'landing'],
    ['page7', 'wed'],
    ['page8', 'wordpress'],
    ['page9', 'web'],
    ['page10', 'graphic'],
    ['page11', 'landing'],
    ['page12', 'wordpress']];
const loader = document.querySelector('.spinner');

function holdAction() {
    //hide spinner
    loader.classList.remove('active-spinner');

    //load pics
    hidePics.forEach((item) => {

        const newElement = `<div class="pic-container ${item[1]}">
                <img src="./pics/morePics/${item[0]}.jpg" alt="" class="pic">
                <div class="pic-hover">
                       <span class="circle1"><i class="fa fa-link" aria-hidden="true"></i></span>
                    <span class="circle2"><i class="fa fa-search fa-xs" aria-hidden="true"></i></span>
                    <h3 class="cr-des">Creative design</h3>
                    <h5 class="w-des">web design</h5>
                </div> </div>`;
        container.innerHTML += newElement;
    });
}

function loadMorePics() {
    //remove button
    loadButton.remove();
    //show spinner
    loader.classList.add('active-spinner');
    //timeout 1-2sec
    setTimeout(holdAction, 2500);
}

loadButton.addEventListener('click', loadMorePics);

/*tab-menu filter in section our amazing work*/
function imageFilter(event) {
    const filter = event.target.getAttribute('data-filter');
    const containers = document.querySelectorAll('.pic-container');
    const tabTitles = document.querySelectorAll('.work-menu>.tab-title');

    tabTitles.forEach(function (elem) {
        elem.classList.remove('active');
    });
    event.target.classList.add('active');


    containers.forEach(function (elem) {
        if (filter === 'all') {
            elem.classList.remove('hidden');
        } else if (!elem.classList.contains(filter)) {
            elem.classList.add('hidden');
        } else {
            elem.classList.remove('hidden');
        }
    })
}

document.querySelector('.work-menu').addEventListener('click', imageFilter);

/*section what people say about*/
const pictures = document.querySelectorAll('.list-container');
const quotes = document.querySelectorAll('.pop-up-pic');
const arrows = document.querySelectorAll('.arrow');

function onPictureClick(event) {
    pictures.forEach(function (elem) {
        elem.classList.remove('current');
    });
    event.currentTarget.classList.add('current');
    const index = event.currentTarget.getAttribute('data-id');
    quoteChange(index);
}

function quoteChange(idx) {
    quotes.forEach(function (elem) {
        elem.classList.remove('visible-quote');
    });
    quotes.item(idx).classList.add('visible-quote');
}

pictures.forEach(function (elem) {
    elem.addEventListener('click', onPictureClick);
});
//arrows
//1 find current active pic and get index
//2 remove class current from this element
//3 depends on what arrow was clicked, add or minus 1 form current index
// 4 call quotaChange function with new index


function arrowClick(event) {
    const left = event.currentTarget.classList.contains('left');
    let idx = 0; //обьявление за пределами цыкла. чтоб использовать вне цыкла
    for (let i = 0; i < pictures.length; i++) {
        if (pictures.item(i).classList.contains('current')) { //выбрать из коллекции елемент(item), и взять его параметры
            pictures.item(i).classList.remove('current');
            idx = i;
            break;
        }
    }
    if (left) {
        if (idx === 0) {
            idx = pictures.length - 1;
        } else {
            idx--;
        }
    } else {
        if (idx === pictures.length - 1) {
            idx = 0;
        } else {
            idx++;
        }
    }
    pictures.item(idx).classList.add('current');
    quoteChange(idx);
}

arrows.forEach(function (elem) {
    elem.addEventListener('click', arrowClick);
});

