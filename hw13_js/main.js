// //var1
// const changeButton = document.querySelector('.theme-change');
//
// function changeTheme(event) {
//     document.body.classList.toggle('dark');
//     document.querySelector('.menu-top').classList.toggle('dark');
//     document.querySelector('.menu').classList.toggle('dark');
//     document.querySelector('.image-container').classList.toggle('dark');
//     document.querySelector('.content').classList.toggle('dark');
//
//     //если ф-ция не была вызвана через event, event =undefined.когда на сайте сохраненная темная тема
//     if (event === undefined) return;
//
//     //то тогда заходит в єтот if и меняем значение в локал сторедж
//     if (localStorage.getItem('theme') !== 'dark') {
//        localStorage.setItem('theme', 'dark');
//    } else {
//        localStorage.setItem('theme', 'light');
//    }
// }
// changeButton.addEventListener('click', changeTheme);
//
// if (localStorage.getItem('theme') === 'dark') {
//     changeTheme();
// }

//var2
const changeButton = document.querySelector('.theme-change');

function changeTheme() {
    document.body.classList.toggle('dark');
    document.querySelector('.menu-top').classList.toggle('dark');
    document.querySelector('.menu').classList.toggle('dark');
    document.querySelector('.image-container').classList.toggle('dark');
    document.querySelector('.content').classList.toggle('dark');
}

function handleLocalStorage() {
    if (localStorage.getItem('theme') !== 'dark') {
        localStorage.setItem('theme', 'dark');
    } else {
        localStorage.setItem('theme', 'light');
    }
}

function changeThemeStorage() {
    changeTheme();
    handleLocalStorage();
}

changeButton.addEventListener('click', changeThemeStorage);

if (localStorage.getItem('theme') === 'dark') {
    changeTheme();
}