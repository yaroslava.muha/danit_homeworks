const passwordForm = document.querySelector('.password-form');
const iconPassword = document.querySelectorAll('.icon-password');
const inputs = document.querySelectorAll(".password-form input");
let warning = document.querySelector(".warning-container");

function showPassword(event) {
    if (event.target.classList.contains('fa-eye')){
        event.target.classList.remove('fa-eye');
        event.target.classList.add('fa-eye-slash');
        event.target.previousElementSibling.setAttribute('type', 'password');

    } else {
        event.target.classList.remove('fa-eye-slash');
        event.target.classList.add('fa-eye');
        event.target.previousElementSibling.setAttribute('type', 'text');
    }
}
iconPassword.forEach(e => e.addEventListener('click', showPassword));

function onSubmit(event) {
    event.preventDefault();
    warning.innerHTML = '';
    if(inputs.item(0).value === inputs.item(1).value) {
        alert('You are welcome');
    } else {
        warning.innerHTML = `<span class="warning-text">Passwords do not match </span>`;
        warning.style.color = 'red';
    }
}
passwordForm.addEventListener('submit', onSubmit);
