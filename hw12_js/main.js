const images = document.querySelectorAll('.image-to-show');
const buttonOff = document.querySelector('.button-off');
const butPlayAgain = document.querySelector('.play-again');
const time = 10000;
let isActive = true; //flag

function changeImages() {
    // объявляем переменную с индексом следующего потенциально активного элемента
    let idx = 0;
    // бежим по всем картинкам и ищем текущий активный элемент
    for (let i = 0; i < images.length; i++) {
        // получаем индекс следующего єлемента
        idx++;
        // проверяем является ли текущий єлемент активным
        if (images.item(i).classList.contains('active')) {
            // если он активный - удаляем этот класс
            images.item(i).classList.remove('active');
            // проверка на то, что индекс следующего элемента не выходит за рамки исходного массива
            if (idx === images.length) {
                // если выходит, возвращаемся на его начало
                idx = 0;
            }
            // элементу под полученным индексом добавляем активный класс
            images.item(idx).classList.add('active');
            // завершаем цикл
            break;
        }
    }
}

let interval = setInterval(changeImages, time);

buttonOff.addEventListener('click', clear);

function clear() {
    isActive = false;
    clearInterval(interval);
}

butPlayAgain.addEventListener('click', restart);

function restart() {
    if (!isActive) {
        //перезапустить выполение функция показывать с картинки с классом active
        interval = setInterval(changeImages, time);
        isActive = true;
    }
}
