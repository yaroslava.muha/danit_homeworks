//основное задание
// let arr = ['hello', 'world', 'Kiev', 'Kharkiv', 'Odessa', 'Lviv', 55, ["inner array"]];
//
// function generateList (array) {
//     return array.map(function (elem) { // возвращается результат масива после обработки мар
//         return `<li>${elem}</li>`;
//     });
// }
// let arrayList = generateList(arr); // положить результат выполнения фун-и в переменную
//
// console.log(arrayList);
// arrayList.forEach(function (elem) {
//     document.querySelector("#list").innerHTML += elem;
// });

// с доп.заданием
let arr = ['hello', 'world', 'Kiev', 'Kharkiv', 'Odessa', 'Lviv', 55, ["inner array", "inner 2"]];

function generateList (array) {  // возвращает все одной строкой
    let arrList = array.map(function (elem) { // шаг1 = ["<li>hello</li>", "<li>world</li>"]
        // <li><ul>......</ul></li>
        if (Array.isArray(elem)) {
          let innerArrayList = generateList(elem);
          return `<li>${innerArrayList}</li>`;
        }
        return `<li>${elem}</li>`;
    });
    let listString = arrList.join(""); //step2  "<li>hello</li><li>world</li>"
    return `<ul>${listString}</ul>`;
}
let list = generateList(arr);

console.log(list);
document.querySelector("#list-container").innerHTML = list;

