// function filterBy(arr, type) {
//     let arr2 = [];
//     for (let i = 0; i < arr.length; i++) {
//         if (typeof arr[i] !== type) {
//             arr2.push(arr[i]);
//         }
//     }
//     return arr2;
// }
//
// let arr = ['hello', 'world', 23, '23', null];
// console.log(filterBy(arr, 'object'));


let arr = ['hello', 'world', 23, '23', null];

function filterBy(arr, type) {
    return arr.filter((value => typeof value !== type))
}

console.log(filterBy(arr, "string"));

