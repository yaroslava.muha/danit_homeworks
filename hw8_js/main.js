let priceInput = document.querySelector("#price-input");
let eventContainer = document.querySelector('#event-container');
let warning = document.querySelector('#warning');

priceInput.addEventListener('focus', onFocus);
priceInput.addEventListener('blur', onBlur);

function onClick() {
    eventContainer.innerHTML = '';
    priceInput.value = '';
    priceInput.style.color = '';
}

function onFocus(event) {
    event.target.style.borderColor = 'green';
}

function onBlur(event) {
    event.target.style.borderColor = '';
    event.target.style.outline = '';
    if (priceInput.value > 0) {
        priceInput.style.color = `green`;
        eventContainer.innerHTML = `<span class="current-price">Current price: ${priceInput.value} <span class="closed-button">X</span></span>`;
        document.querySelector('.closed-button').addEventListener('click', onClick);
        warning.innerHTML = '';
    } else if (priceInput.value < 0) {
        priceInput.style.borderColor = 'red';
        warning.innerHTML = `<span class="warning-text">Please enter correct price</span>`;
        priceInput.style.color = ``;
        eventContainer.innerHTML = '';
    }
}