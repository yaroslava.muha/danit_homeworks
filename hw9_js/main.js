const tabs = document.querySelector('.tabs');
const tabsContent = document.querySelector('.tabs-content');

function onClickTabs(event) {
    const tabsTitle = document.querySelectorAll('.tabs-title');
    const tabText = document.querySelectorAll('.tab-text');

    const tabColorChange = tabs.querySelector('.active');
    tabColorChange.classList.remove('active');
    event.target.classList.add('active');
    const textVisibilityChange = tabsContent.querySelector('.visible');
    textVisibilityChange.classList.remove('visible');
    for (let i = 0; i < tabsTitle.length; i++) {
        if (tabsTitle.item(i).classList.contains('active')) {
            tabText.item(i).classList.add('visible');
            break;
        }
    }
}
tabs.addEventListener('click', onClickTabs);

