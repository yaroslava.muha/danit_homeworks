let m, n;
do {
    m = prompt("enter 1st number", m);
    n = prompt("enter 2d number", n);
} while (m % 1 !== 0 && n % 1 !== 0 || m === "" && n === "" || (isNaN(m)) && (isNaN(n)));

m = parseInt(m); //преобразует строку в целое число//
n = parseInt(n);

if (m > n) { // если первое число больше второго, то поменять местами значение в переменных с использованием временной переменной//
    let temp = n;
    n = m;
    m = temp;
}
for (let i = m; i <= n; i++) {
    let isPrime = true;
    for (let j = 2; j < i; j++) {
        if (i % j === 0) {
            isPrime = false;
            break;
        }
    }
    if (isPrime) {
        console.log(i + " is prime")
    }
}
